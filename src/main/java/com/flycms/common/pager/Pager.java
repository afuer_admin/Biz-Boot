package com.flycms.common.pager;

import java.io.Serializable;
import java.util.List;
/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: mybatis分页处理类
 * @email 79678111@qq.com
 * @Date: 1:33 2019/8/29
 */
public class Pager<T> implements Serializable {

    private List<T> list; //对象记录结果集
    private int total; // 总记录数
    private int pageSize; // 每页显示数量
    private int pageNumber; // 当前页
    private int startRow; // 开始角标
    private int endRow; // 结束角标
    private String whereStr = ""; //查询条件

    public static final boolean ASC = true;
    public static final boolean DESC = false;
    private static final String KW_ORDER_BY     = " ORDER BY ";
    private static final String KW_ASC          = " ASC ";
    private static final String KW_DESC         = " DESC ";
    private static final String COMMA           = " , ";

    /**
     * order by子句
     */
    private StringBuilder orderByClause = new StringBuilder();

    /**
     * limit 查询语句， mybatis里的xml输出结构limit #{startRow},#{endRow}
     */
    private StringBuilder limitByClause = new StringBuilder();

    //防注入使用实体类传递参数
    private Object entity;

    public Pager() {
        this.pageNumber = 1;
        this.pageSize = 20;
        this.startRow = pageNumber > 0 ? (pageNumber - 1) * pageSize : 0;
        this.endRow = pageNumber * pageSize;
    }

    /**
     * 翻页条件设置
     *
     * @param pageNumber
     *        当前页码数
     * @param pageSize
     *        每页显示列数
     */
    public Pager(Integer pageNumber, Integer pageSize) {
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
        this.startRow = pageNumber > 0 ? (pageNumber - 1) * pageSize : 0;
        this.endRow = pageNumber > 0 ? pageNumber * pageSize : pageSize;
    }

    public Pager(Integer pageNumber, Integer pageCount, Integer total){
        this.pageNumber = pageNumber;
        this.pageSize = pageCount;
        this.total = total;
        this.startRow = pageNumber > 0 ? (pageNumber - 1) * pageCount : 0;
        this.endRow = pageNumber > 0 ?
                (pageNumber * pageCount > total ? total	: pageNumber * pageCount)
                : (pageCount > total ? total : pageCount);
    }

    /**
     * 按参数查询分页列表
     *
     * @param pageNumber
     *        当前页码数
     * @param pageSize
     *        当前列表页结果集
     * @param whereStr
     *        wher查询条件，如： and 1 = 1
     * @param sortStr
     *        排序条件
     */
    public Pager(Integer pageNumber, Integer pageSize, String whereStr, String sortStr) {
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
        this.startRow = pageNumber > 0 ? (pageNumber - 1) * pageSize : 0;
        this.endRow = pageNumber * pageSize;
        this.whereStr = whereStr;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    /**
     * 得到当前页的内容
     * @return {List}
     */
    public List<T> getList() {
        return list;
    }

    /**
     * 得到记录总数
     * @return {int}
     */
    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageCount) {
        this.pageSize = pageCount;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getStartRow() {
        return startRow;
    }

    public void setStartRow(int startRow) {
        this.startRow = startRow;
    }

    public int getEndRow() {
        return endRow;
    }

    public void setEndRow(int endRow) {
        this.endRow = endRow;
    }

    public String getWhereStr() {
        return whereStr;
    }

    public Object getEntity() {
        return entity;
    }

    public void setEntity(Object entity) {
        this.entity = entity;
    }

    public void setWhereStr(String whereStr) {
        this.whereStr = whereStr;
    }

    /**
     * 拼接order by子句
     * @param propertyName 参与排序的属性名
     * @param order true表示升序，false表示降序
     * @param status true 开，false 关
     */
    public StringBuilder addOrderProperty(String propertyName, boolean order,boolean status){
        if(status){
            if(orderByClause.length() == 0){
                orderByClause = new StringBuilder(KW_ORDER_BY).append(propertyName + (order ? KW_DESC : KW_ASC));
            } else{
                orderByClause.append(COMMA).append(propertyName + (order ? KW_DESC : KW_ASC));
            }
        }
        return orderByClause;
    }

    /**
     * 拼接Limit 语句
     * @param status true开，false关
     */
    public StringBuilder addLimitProperty(boolean status){
        if(status){
            limitByClause.append(" limit "+startRow+","+pageSize);
        }
        return limitByClause;
    }
}
