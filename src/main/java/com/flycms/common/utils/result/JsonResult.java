package com.flycms.common.utils.result;

import java.util.HashMap;
import java.util.Map;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: ${Description}
 * @email 79678111@qq.com
 * @Date: 15:41 2019/10/17
 */
public class JsonResult {

    /**
     * RestControllerHelper的toJson常量
     */
    private static final String RESULT_SUCCESS = "status";
    private static final String RESULT_CODE = "code";
    private static final String RESULT_MSG = "msg";
    private static final String RESULT_DATA = "data";

    /**
     * 200: 成功。
     * 401: 当前请求需要用户验证。
     * 403：权限错误。
     * 404: 请求的资源未找到。
     * 408：请求超时。
     */
    public static final int SUCCESS = 200;
    public static final int UNLOGIN = 401;
    public static final int FORBIDDEN = 403;
    public static final int NOT_FOUND = 404;
    public static final int TIMEOUT = 408;

    /**
     *  code: 状态码
     *  msg: 状态码消息
     *  data: 数据
     */
    private static Integer code;
    private static String msg;
    private static Object data;

    public JsonResult() {
        this.code = SUCCESS;
    }

    public JsonResult(Integer code,Object data) {
        this.code = code;
        this.data = data;
    }

    public JsonResult(Integer code,Object data,String msg) {
        this.code = code;
        this.data = data;
        this.msg = msg;
    }

    public JsonResult(Object data) {
        this.code = code;
        this.data = data;
        this.msg = msg;
    }

    /**
     * statusMap
     * @return
     */
/*    public static Map<String,Object> statusMap(Integer code,Object data) {
        Map<String,Object> map = new HashMap<>();
        Map<String,Object> status = new HashMap<>();
        status.put(RESULT_CODE,this.code);
        status.put(RESULT_MSG,this.msg);
        map.put(RESULT_SUCCESS,status);
        map.put(RESULT_DATA,data);
        return map;
    }*/

    /**
     * toJsonMap
     * @return
     */
    public Map<String,Object> toJsonMap() {
        Map<String,Object> map = new HashMap<>();
        map.put(RESULT_CODE,this.code);
        map.put(RESULT_MSG,this.msg);
        map.put(RESULT_DATA,this.data);
        return map;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}