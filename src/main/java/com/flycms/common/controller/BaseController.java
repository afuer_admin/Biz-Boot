package com.flycms.common.controller;

import com.flycms.common.constant.SiteConstants;
import com.flycms.common.utils.web.ServletUtils;
import com.flycms.common.utils.StringUtils;
import com.flycms.common.utils.result.Result;
import com.flycms.modules.company.entity.Company;
import com.flycms.modules.company.service.CompanyService;
import com.flycms.modules.help.service.HelpService;
import com.flycms.modules.links.service.LinksService;
import com.flycms.modules.news.service.NewsService;
import com.flycms.modules.product.service.ProductService;
import com.flycms.modules.shiro.ShiroUtils;
import com.flycms.modules.site.service.SiteColumnService;
import com.flycms.modules.site.service.SiteCompanyService;
import com.flycms.modules.site.service.SiteService;
import com.flycms.modules.system.service.ConfigureService;
import com.flycms.modules.template.service.SiteTemplateService;
import com.flycms.modules.template.service.TemplateService;
import com.flycms.modules.user.entity.Admin;
import com.flycms.modules.user.entity.User;
import com.flycms.modules.user.service.*;
import com.flycms.modules.visit.service.VisitService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: Controller基类，做为所有Controller页面引用父类
 * @email 79678111@qq.com
 * @Date: 1:33 2019/8/29
 */
public class BaseController{

	@Autowired
	protected TemplateService theme;
	@Autowired
	protected HttpServletRequest request;
	@Autowired
	protected HttpSession session;
	@Autowired
	protected UserService userService;
	@Autowired
	protected CompanyService companyService;
	@Autowired
	protected UserRoleService userRoleService;
	@Autowired
	protected UserMenuService userMenuService;
	@Autowired
	protected AdminService adminService;
	@Autowired
	protected AdminRoleService adminRoleService;
	@Autowired
	protected AdminMenuService adminMenuService;
	@Autowired
	protected SiteConstants siteConstants;
	@Autowired
	protected SiteService siteService;
	@Autowired
	protected ConfigureService systemService;
	@Autowired
	protected TemplateService templateService;
	@Autowired
	protected SiteTemplateService siteTemplateService;
    @Autowired
    protected SiteColumnService siteColumnService;
    @Autowired
    protected SiteCompanyService siteCompanyService;
    @Autowired
    protected NewsService newsService;
    @Autowired
    protected ProductService productService;
	@Autowired
	protected VisitService visitService;
	@Autowired
	protected LinksService linksService;
	@Autowired
	protected HelpService helpService;

	/**
	 * 获取request
	 */
	public HttpServletRequest getRequest()
	{
		return ServletUtils.getRequest();
	}

	/**
	 * 获取response
	 */
	public HttpServletResponse getResponse()
	{
		return ServletUtils.getResponse();
	}

	/**
	 * 获取session
	 */
	public HttpSession getSession()
	{
		return getRequest().getSession();
	}

	/**
	 * 返回成功
	 */
	public Result success()
	{
		return Result.success();
	}

	/**
	 * 返回失败消息
	 */
	public Result failure()
	{
		return Result.failure();
	}

	/**
	 * 返回成功消息
	 */
	public Result success(String message)
	{
		return Result.success(message);
	}

	/**
	 * 返回成功消息和指定跳转url
	 */
	public Result success(String msg,String goUrl)
	{
		return Result.success(msg, goUrl);
	}

	/**
	 * 返回失败消息
	 */
	public Result failure(String message)
	{
		return Result.failure(message);
	}

	/**
	 * 返回错误码消息
	 */
	public Result failure(int code, String message)
	{
		return Result.failure(code, message);
	}


	/**
	 * 根据shiro内的用户id查询前台用户信息
	 *
	 * @return
	 */
	public User getUser()
	{
		User user = null;
		if (StringUtils.isNotNull(ShiroUtils.getLoginUser()))
		{
			user = userService.findById(ShiroUtils.getLoginUser().getId());
		}
		return user;
	}

	/**
	 * 根据shiro内的用户id查询后台管理员信息
	 *
	 * @return
	 */
	public Admin getAdmin()
	{
		Admin user = null;
		if (StringUtils.isNotNull(ShiroUtils.getLoginUser()))
		{
			user = adminService.findById(ShiroUtils.getLoginUser().getId());
		}
		return user;
	}

	/**
	 * 根据shiro内的用户id查询企业信息
	 *
	 * @return
	 */
	public Company getCompany()
	{
		Company company = null;
		if (StringUtils.isNotNull(ShiroUtils.getLoginUser()))
		{
			company = companyService.findCompanyByUser(ShiroUtils.getLoginUser().getId());
		}
		return company;
	}
}
