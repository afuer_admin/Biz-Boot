package com.flycms.modules.picture.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: ${Description}
 * @email 79678111@qq.com
 * @Date: 16:36 2019/11/4
 */
@Setter
@Getter
public class Picture implements Serializable {
    private static final long serialVersionUID = 1L;
    /** 图片编号 */
    public Long id;
    /** 网站id */
    private Long companyId;
    /** 用户id */
    public Long userId;
    /**  图片地址 */
    public String imgUrl;
    /** 图片名称 */
    public String imgName;
    /** 图片说明 */
    public String description;
    /** 图片体积大小 */
    public String fileSize;
    /** 图片宽度 */
    public String imgWidth;
    /** 图片高度 */
    public String imgHeight;
    /** 图片指纹 */
    public String signature;
    /** 添加时间 */
    public LocalDateTime createTime;
    /** 该图片使用次数 */
    private Integer countInfo;
    /** 排序 */
    public Integer sort;
    /** 逻辑删除设置 */
    public String deleted;
}
