package com.flycms.modules.notify.dao;

import com.flycms.common.dao.BaseDao;
import com.flycms.modules.notify.entity.SmsApi;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 开发公司：28844.com<br/>
 * 版权：28844.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 14:15 2018/7/19
 */
@Repository
public interface SmsApiDao extends BaseDao<SmsApi> {

    // ///////////////////////////////
    // /////      增加        ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////
    //按id更新手机短信API接口信息
    public int updagteSmsapiById(SmsApi smsApi);

    // ///////////////////////////////
    // /////       查询       ////////
    // ///////////////////////////////



}
