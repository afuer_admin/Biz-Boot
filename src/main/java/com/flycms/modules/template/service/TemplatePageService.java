package com.flycms.modules.template.service;

import com.flycms.modules.template.entity.TemplatePage;
import org.springframework.transaction.annotation.Transactional;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: ${Description}
 * @email 79678111@qq.com
 * @Date: 3:00 2019/9/12
 */
public interface TemplatePageService {
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////
    public Object addDeveloperTemplatePage(TemplatePage templatePage);
    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////

    /**
     * 开发人员编辑模板信息
     *
     * @param templatePage
     * @return
     */
    public Object updateDeveloperTemplatePage(TemplatePage templatePage);

    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    /**
     * 按id查询模板信息
     *
     * @param id
     * @return
     */
    public TemplatePage findById(Long id);

    /**
     * 查询当前模板拥有的所有模板页面
     *
     * @param templatePage
     * @param page
     * @param pageSize
     * @param sort
     * @param order
     * @return
     */
    public Object selectTemplatePagePager(TemplatePage templatePage, Integer page, Integer pageSize, String sort, String order);
}
