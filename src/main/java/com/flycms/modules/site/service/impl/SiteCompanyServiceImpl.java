package com.flycms.modules.site.service.impl;


import com.flycms.common.utils.mark.SnowFlakeUtils;
import com.flycms.common.utils.result.Result;
import com.flycms.modules.company.entity.Company;
import com.flycms.modules.company.service.CompanyService;
import com.flycms.modules.shiro.ShiroUtils;
import com.flycms.modules.site.dao.SiteCompanyDao;
import com.flycms.modules.site.entity.SiteCompany;
import com.flycms.modules.site.service.SiteCompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

/**
 * Open source house, All rights reserved
 * 版权：28844.com<br/>
 * 开发公司：28844.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 2:08 2019/8/27
 */
@Service
public class SiteCompanyServiceImpl implements SiteCompanyService {
    @Autowired
    private SiteCompanyDao siteCompanyDao;
    @Autowired
    private CompanyService companyService;
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////
    @Transactional
    public Object addSiteCompany(SiteCompany siteCompany){
        if (this.checkSiteCompany(siteCompany.getSiteId())){
            return Result.failure("该网站已被其他用户绑定！您无法绑定！");
        }
        siteCompany.setId(SnowFlakeUtils.nextId());
        return siteCompanyDao.save(siteCompany);
    }


    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////
    /**
     * 按网站id删除网站与企业关联信息
     *
     * @param siteId
     *         网站id
     * @return
     */
    public int deleteSiteCompanyById(Long[]  siteId){
        return siteCompanyDao.deleteSiteCompanyById(siteId);
    }
    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////



    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////

    /**
     * 查询用户是否拥有该网站管理权限
     * 通过用户查询关联的企业信息，用企业id和网站id查询是否关联
     *
     * @param siteId
     * @return
     */
    public boolean checkSiteCompany(Long siteId) {
        Company company=companyService.findCompanyByUser(ShiroUtils.getLoginUser().getId());
        if(company==null){
            return false;
        }
        int totalCount = siteCompanyDao.checkSiteCompany(company.getId(),siteId);
        return totalCount > 0 ? true : false;
    }

    /**
     * 企业id查询当前拥有的网站数量
     *
     * @param companyId
     * @return
     */
    public Set<Long> querySiteIdList(Long companyId){
        return siteCompanyDao.querySiteIdList(companyId);
    }
}