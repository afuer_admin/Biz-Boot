package com.flycms.modules.site.controller;

import com.flycms.common.controller.BaseController;
import com.flycms.common.exception.ApiAssert;
import com.flycms.common.utils.result.LayResult;
import com.flycms.common.validator.Sort;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 公共的
 * @author 孙开飞
 */
@Controller
@RequestMapping("/admin/column")
public class ColumnAdminController extends BaseController {

    /**
     * 网站分类列表
     *
     * @return
     */
    @GetMapping("/list{url.suffix}")
    public String siteList(@RequestParam(value = "siteid", defaultValue = "0") String siteid,
                           @RequestParam(value = "id", defaultValue = "0") String id, Model model)
    {
        ApiAssert.notTrue(siteid == null || !NumberUtils.isNumber(siteid), "网站id为空或者类型错误");
        model.addAttribute("siteId",siteid);
        return "system/member/site/column/list";
    }

    @GetMapping("/listData{url.suffix}")
    @ResponseBody
    public Object siteList(@RequestParam(value = "siteid", defaultValue = "0") String siteid,
                           @RequestParam(value = "page",defaultValue = "1") Integer page,
                           @RequestParam(value = "pageSize",defaultValue = "10") Integer pageSize,
                           @Sort(accepts = {"add_time", "id"}) @RequestParam(defaultValue = "add_time") String sort,
                           @RequestParam(defaultValue = "desc") String order) {
        ApiAssert.notTrue(siteid == null || !NumberUtils.isNumber(siteid), "网站id为空或者类型错误");
        return LayResult.success(0,"true", 0, siteColumnService.selectColumnListTreetable(Long.parseLong(siteid)));
    }
}
