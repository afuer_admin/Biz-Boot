package com.flycms.modules.help.controller;

import com.flycms.common.controller.BaseController;
import com.flycms.modules.help.service.HelpService;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 帮助说明
 * @author 孙开飞
 */
@Controller
@RequestMapping("/help/")
public class HelpFrontController extends BaseController {
    @Autowired
    private HelpService helpService;
	/**
	 * 使用入门
	 */
	@RequestMapping("/list{url.suffix}")
	public String helpList(String siteid, String title,
                           @RequestParam(defaultValue = "1") Integer page,
                           @RequestParam(defaultValue = "10") Integer limit,
                           @RequestParam(defaultValue = "add_time") String sort,
                           @RequestParam(defaultValue = "desc") String order,Model model){
        Object pager=helpService.queryHelpPager(NumberUtils.toLong(siteid),title,page,limit,sort,order);
        model.addAttribute("siteid", siteid);
        model.addAttribute("title", title);
        model.addAttribute("pager",pager);
		return "admin/help/list";
	}

	
	
}
