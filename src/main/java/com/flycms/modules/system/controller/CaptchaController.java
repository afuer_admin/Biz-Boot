package com.flycms.modules.system.controller;

import com.flycms.common.constant.UserConstants;
import com.flycms.common.controller.BaseController;
import com.flycms.modules.shiro.ShiroUtils;
import com.google.code.kaptcha.Constants;
import com.google.code.kaptcha.Producer;
import com.wf.captcha.ArithmeticCaptcha;
import com.wf.captcha.ChineseCaptcha;
import com.wf.captcha.GifCaptcha;
import com.wf.captcha.SpecCaptcha;
import com.wf.captcha.utils.CaptchaUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 图片验证码（支持算术形式）
 * 
 * @author 孙开飞
 */
@Controller
@RequestMapping("/captcha")
public class CaptchaController extends BaseController
{

    /**
     * 生成算术验证码
     * @param response
     * @throws IOException
     */
    @RequestMapping("/arithmetic_captcha{url.suffix}")
    public void ArithmeticCaptcha(HttpServletRequest request, HttpServletResponse response) throws Exception {
        // 算术类型
        ArithmeticCaptcha captcha = new ArithmeticCaptcha(130, 48);
        captcha.setLen(2);  // 几位数运算，默认是两位
        captcha.getArithmeticString();  // 获取运算的公式：3+2=?
        captcha.text();  // 获取运算的结果：5
        CaptchaUtil.out(captcha, request, response);  // 输出验证码
    }

    /**
     * 生成中文验证码
     * @param response
     * @throws IOException
     */
    @RequestMapping("/chinese_captcha{url.suffix}")
    public void ChineseCaptcha(HttpServletRequest request, HttpServletResponse response) throws Exception {
        // 算术类型
        ChineseCaptcha captcha = new ChineseCaptcha(130, 48);
        captcha.setLen(4);  // 几位数运算，默认是两位
        captcha.text();  // 获取运算的结果：5
        CaptchaUtil.out(captcha, request, response);  // 输出验证码
    }

    /**
     * 生成gif验证码
     * @param response
     * @throws IOException
     */
    @RequestMapping("/gif_captcha{url.suffix}")
    public void GifCaptcha(HttpServletRequest request, HttpServletResponse response) throws Exception {
        GifCaptcha captcha = new GifCaptcha(130, 48);
        captcha.setLen(4);  // 几位数运算，默认是两位
        CaptchaUtil.out(captcha, request, response);
    }
    /**
     * 生成随机验证码
     * @param response
     * @throws IOException
     */
    @RequestMapping("/captcha{url.suffix}")
    public void RandomsCaptcha(HttpServletRequest request, HttpServletResponse response) throws Exception {
        // png类型
        SpecCaptcha captcha = new SpecCaptcha(130, 48);
        captcha.text();  // 获取验证码的字符
        captcha.textChar();  // 获取验证码的字符数组
        CaptchaUtil.out(captcha, request, response);  // 输出验证码
    }
}