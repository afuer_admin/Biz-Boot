package com.flycms.modules.system.service.impl;

import com.flycms.common.pager.Pager;
import com.flycms.common.utils.mark.SnowFlakeUtils;
import com.flycms.common.utils.result.LayResult;
import com.flycms.common.utils.result.Result;
import com.flycms.modules.system.dao.SystemModuleDao;
import com.flycms.modules.system.entity.SystemModule;
import com.flycms.modules.system.service.SystemModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 1:52 2019/8/25
 */
@Service
public class SystemModuleServiceImpl implements SystemModuleService {
    @Autowired
    private SystemModuleDao systemModuleDao;
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////
    /**
     * 添加模块信息
     *
     * @param systemModule
     * @return
     */
    @Transactional
    public Result addModule(SystemModule systemModule){
        if(this.checkModuleByNameAndType(systemModule.getModuleName(),systemModule.getModuleType())){
            return Result.failure("该模块已存在！");
        }
        systemModule.setId(SnowFlakeUtils.nextId());
        systemModule.setAddTime(LocalDateTime.now());
        systemModuleDao.save(systemModule);
        return Result.success();
    }


    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    /**
     * 删除系统模块信息
     *
     * @param id
     * @return
     */
    public int deleteById(Long id){
        return systemModuleDao.deleteById(id);
    }
    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////



    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    /**
     * 查询当前模块是否已占用
     *
     * @param moduleName
     * @param moduleType
     * @return
     */
    public boolean checkModuleByNameAndType(String moduleName,String moduleType) {
        int totalCount = systemModuleDao.checkModuleByNameAndType(moduleName,moduleType);
        return totalCount > 0 ? true : false;
    }

    /**
     * 按id查询模块信息
     *
     * @param id
     * @return
     */
    public SystemModule findById(Long id){
        return systemModuleDao.findById(id);
    }

    /**
     * 查询用户所有网站列表
     *
     * @return
     */
    public List<SystemModule> selectModuleList() {
        StringBuffer whereStr = new StringBuffer();
        whereStr.append(" status = 1");
        whereStr.append(" and deleted = 0");
        Pager<SystemModule> pager = new Pager();
        //排序设置
        pager.addOrderProperty("sort_order", false,true);
        //使用limit进行查询翻页
        pager.addLimitProperty(false);
        //查询条件
        SystemModule entity = new SystemModule();
        pager.setEntity(entity);
        pager.setWhereStr(whereStr.toString());
        List<SystemModule> sitelsit = systemModuleDao.queryList(pager);
        return sitelsit;
    }

    /**
     * 查询模型翻页列表
     *
     * @param module
     * @param page
     * @param pageSize
     * @param sort
     * @param order
     * @return
     */
    public Object selectModulePager(SystemModule module, Integer page, Integer pageSize, String sort, String order) {
        StringBuffer whereStr = new StringBuffer(" 1 = 1");
        if (!StringUtils.isEmpty(module.getModuleName())) {
            whereStr.append(" and module_name like concat('%',#{entity.moduleName},'%')");
        }
        whereStr.append(" and deleted = 0");
        if (!StringUtils.isEmpty(module.getModuleType())) {
            whereStr.append(" and module_type = #{entity.moduleType}");
        }
        if (!StringUtils.isEmpty(module.getStatus())) {
            whereStr.append(" and status = #{entity.status}");
        }
        Pager<SystemModule> pager = new Pager(page, pageSize);
        //排序设置
        if (!StringUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        //查询条件
        SystemModule entity = new SystemModule();
        entity.setModuleName(module.getModuleName());
        entity.setModuleType(module.getModuleType());
        entity.setStatus(module.getStatus());
        pager.setEntity(entity);
        pager.setWhereStr(whereStr.toString());
        List<SystemModule> sitelsit = systemModuleDao.queryList(pager);
        return LayResult.success(0, "true", systemModuleDao.queryTotal(pager), sitelsit);
    }

}
