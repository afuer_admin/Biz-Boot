package com.flycms.modules.system.dao;

import com.flycms.common.dao.BaseDao;
import com.flycms.modules.system.entity.Configure;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 10:08 2019/6/9
 */
@Repository
public interface ConfigureDao extends BaseDao<Configure> {
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////
    /**
     * 批量删除配置信息
     *
     * @param id 需要删除的数据ID
     * @return 结果
     */
    public int deleteByIds(Long[] id);
    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////
    /**
     * 按id修改系统配置信息
     *
     * @param system 系统配置信息
     * @return 结果
     */
    public int updateSystem(Configure system);

    /**
     * 按keyCode修改系统配置keyValue信息
     *
     * @param keyCode
     * @param keyValue
     * @return 结果
     */
    public int updateKeyValue(@Param("keyCode") String keyCode,@Param("keyValue") String keyValue);
    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////

    /**
     * 查询系统变量keyCode是否有重复
     *
     * @param keyCode
     * @param id 需要排除id
     * @return
     */
    public int checkSystemByKeycode(@Param("keyCode") String keyCode,@Param("id") Long id);


    /**
     * 按keyCode查询对应的value信息
     *
     * @param keyCode
     * @return
     */
    public Configure findByKeyCode(@Param("keyCode") String keyCode);

    /**
     * 查询全部系统变量
     *
     * @return
     */
    public List<Configure> queryAllList();

}
