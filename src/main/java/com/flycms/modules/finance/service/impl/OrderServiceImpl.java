package com.flycms.modules.finance.service.impl;

import com.flycms.common.pager.Pager;
import com.flycms.modules.finance.dao.OrderDao;
import com.flycms.modules.finance.entity.Order;
import com.flycms.modules.finance.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Set;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 23:53 2019/8/17
 */
@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderDao orderDao;

    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////



    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    /**
     * 查询该企业指定产品类型的产品订单id列表
     *
     * @param companyId
     *         企业id
     * @param marketType
     *        产品类型
     * @return
     */
    public Set<Long> queryOrderByProductIdList(Long companyId, Integer marketType){
        return orderDao.queryOrderByProductIdList(companyId,marketType);
    }
    /**
     * 查询订单列表
     *
     * @param siteid
     *         所输网站id
     * @param orderSn
     *         订单号
     * @param page
     *         当前页数
     * @param pageSize
     *         每页显示数量
     * @param sort
     *         指定排序字段
     * @param order
     *         排序方式
     * @return
     */
    public Object queryOrderPager(Long siteid, String orderSn, Integer page, Integer pageSize, String sort, String order) {
        StringBuffer whereStr = new StringBuffer(" 1 = 1");
        if (!StringUtils.isEmpty(siteid) && siteid > 0l) {
            whereStr.append(" and siteid = #{entity.siteid}");
        }
        if (!StringUtils.isEmpty(orderSn)) {
            whereStr.append(" and order_sn = #{entity.orderSn}");
        }
        whereStr.append(" and deleted = 1");

        Pager<Order> pager = new Pager(page, pageSize);
        //排序设置
        if (!StringUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        //查询条件
        Order entity = new Order();
        entity.setCompanyId(siteid);
        entity.setOrderSn(orderSn);
        pager.setEntity(entity);
        pager.setWhereStr(whereStr.toString());
        pager.setList(orderDao.queryList(pager));
        pager.setTotal(orderDao.queryTotal(pager));
        return pager;
    }
}
